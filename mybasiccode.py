class MyPythonclass:
    
    def lstTostr(lst):
        string = ' '
        for i in lst:
            string += ' '.join(i) + ' '   
        return string
   
    if __name__ == "__main__":
        lst = ["hello", "world"]
        print(lstTostr(lst))